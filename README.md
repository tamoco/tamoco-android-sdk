![logo](https://bitbucket.org/repo/XX5xgGx/images/3404555888-logo.gif.png)

### Android Client SDK Integration Guide

This document is written using Android Studio 3.3.2 as IDE.
It covers a basic initialization of the SDK, if you need more information check [customization docs](CUSTOM.md).

If you are migrating from version `1.X.X` check [migration docs](MIGRATION.md) for the migration guide.

### Changelog
To see what has changed between each version of the SDK please check [CHANGELOG.md](CHANGELOG.md)

### Requirements

## Android Studio
[Android Studio](https://developer.android.com/studio/index.html) is now the official IDE for Android. We recommend using the latest, stable Android Studio build to integrate the Tamoco SDK in your application.

## Minimum Android SDK Target
The SDK requires a minimum Android API level of 15 (Android 4.0.3 Ice Cream Sandwich).

```groovy
    minSdkVersion 15
    targetSdkVersion 27
```

**NOTE:** Beacon scanning will only work on devices with Android (4.3 Jelly Bean) and above. This is because BLE scanning requires at least API 18. The SDK gracefully stops beacon scanning if the API level is lower.

## Google Play Services
The Tamoco Android SDK relies on Google Play Services, most precisely on the Locations and Cloud Messaging packages. Any device without Google Play Services will be unable to use any of the SDK features.

The necessary dependencies are automatically included with the SDK out of the box. For more information on manually including the Google Play Services dependencies click [here](https://developers.google.com/android/guides/setup).

If you encounter any version conflicts in the Play Services versions imported by your app, please upgrade them to the latest stable version. The SDK currently uses `12.0.1`.

Example:
```groovy
implementation 'com.google.android.gms:play-services-location:12.0.1'
```

## Support library
com.android.support:support-v4 is included in the SDK as a `compileOnly` dependency please include a version of said library in your project.

Example:
```groovy
implementation 'com.android.support:support-v4:27.1.0'
```

### Getting started

## Integration

To include the SDK dependencies please follow this steps:

1. Add the following to your project root's, `build.gradle` file:

	```groovy
	allprojects {
	    repositories {
	        jcenter()
	        maven {
	            url "https://nexus.tamoco.com/repository/maven-public/"
	        }
	    }
	}
	```

2. Add the following dependency to your module's `build.gradle`:

	```groovy
	dependencies {
	    ...
	    implementation 'com.tamoco.sdk:core:2.1.8'
	    ...
	}
	```

3. Perform a gradle sync and the SDK will then be available in your application.

## Initialization
To use the SDK, you must first initialize it. To do so please follow this steps:

1. Enter the [Apps panel](https://ui.tamoco.com/#!/apps) in the Tamoco Dashboard.

2. Add a new application and selecting `Android` as Device OS.

3. An App ID and App Secret will be generated for your application. This keys can be found in the newly created application's detail page.

4. Include the keys in your `Manifest.xml` as follows:

	```xml
	<application ...>
		...
	    <meta-data android:name="com.tamoco.APP_ID" android:value="{YOUR_APP_ID}"/>
	    <meta-data android:name="com.tamoco.APP_SECRET" android:value="{YOUR_APP_SECRET}"/>
		...
	</application>
	```

5. Create a custom Application class (or use the one you already had set up in your project).

6. Add the following code in your Application's `onCreate` method:

```java
public class MyApplication extends Application {

    ...

    @Override
    public void onCreate() {
        super.onCreate();
        ...
        // Build the basic configuration
        TamocoConfig config = new TamocoConfig.Builder()
            // Scanning results from geofences, beacons and wifi.
            .allKits()
            .build();
        // Initialize the SDK
        Tamoco.with(getApplicationContext(), config);
        ...
    }

    ...
}
```

If you need only one or two kits check : [customization docs](CUSTOM.md).


## Required permissions
The Tamoco SDK requires some permissions in order to run correctly. Certain features might stop working if the permissions are not granted.

### Non-dangerous permissions
This permissions are auto approved by the user just by installing the app and only need to be declaredin your app's `Manifest.xml`:

```xml
    <manifest ...>
        ...
        <!-- Needed to start the SDK when the device boots -->
        <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>
        <!-- Needed to update the inventory -->
        <uses-permission android:name="android.permission.INTERNET"/>

        <!-- Needed for wifi scanning -->
        <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>

        <!-- Needed for beacon scanning -->
        <uses-permission android:name="android.permission.BLUETOOTH"/>
        <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>

        ...
    </manifest>
```
You do not need to include the wifi and bluetooth permissions if you do not intend to scan for wifi networks and beacons respectively.

### Dangerous permissions
This permissions also need to be added to your app's `Manifest.xml`:

```xml
    <manifest ...>
        ...
        <!-- Needed to know the devices current location,
        the SDK will not work until this permission is granted -->
        <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
        ...
    </manifest>
```

However, you also need to request its approval at runtime. The Tamoco SDK offers a helper class to request all the required runtime permissions. We suggest including this requests on your Main or Splash Activity:

```java
public class MyMainActivity extends Activity {
    ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ...
        Tamoco.requestPermissions(this);
        ...
    }
    ...
}
```

You also need to notify the SDK when the permission request results are available so that it can start to work as normal (if the permissions were granted).

```java
@Override
public class MyMainActivity extends Activity {
    ...
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ...
        Tamoco.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        ...
    }
    ...
}
```

Finally, you can always check if the required SDK permissions are granted or not,
even thought you don't need to:

```java
if(Tamoco.hasSDKPermissions(getApplicationContext())){
    // Do something
} else {
    // Do other thing
}
```

You can read more about requesting permissions at Run Time in Google�s documentation: https://developer.android.com/training/permissions/requesting.html

### Customization
If you need a stronger usage of our sdk check : [customization docs](CUSTOM.md).
