![logo](https://bitbucket.org/repo/XX5xgGx/images/3404555888-logo.gif.png)
### Changelog

### v2.2.1
- fix BLUETOOTH permission error

### v2.2.0
- bintray integration
- update libraries and play services to 16.0.0
- rollbar fix
- add consent support

### v2.1.8
- Fix crash in DetectedActivitiesIntentService

### v2.1.7
- allow the publisher to start/stop/startimmediatly the sdk
- override rollbar version code

### v2.1.6
- [ANR] put BluetoothJobService bluetooth callback in thread
- make reportCrashes mandatory / deprecate the public method
- send rollbar app name as environement
- fix crash in DetectedActivitiesIntentService class

### v2.1.5
- fix in our Rollbar analytics tools
- fix bug in sending event to our API
- fix crash in BluetoothScanReceiver class
- fix ANR when app enter in foreground
- fix ANR in BluetoothJobService class
- fix ANR in WifiJobService class

### v2.1.4
- rollback to Java 7
- ANR error not sent to the default handler

### v2.1.3
- add carier data & motion info
- track anr errors in rollbar
- minor bugfixes

### v2.1.2
- Bugfixes on server requests
- Move all BroadacstReceiver in tamoco thread to avoid ANR
- remove listen only mode

### v2.1.0
- bundle all kits in one module

### v2.0.6
- Fixed some bugs on the core module

### v2.0.5
- Updated formatting of data to comply with api modifications
- Stopped throwing TamocoExceptions, instead now fails gracefully

### v2.0.4
- Catching and logging a crash caused by starting the SDK after a reboot.

### v2.0.3
- Fixed a crash caused by the SDK starting and trying to access storage before the storage system has been decrypted

### v2.0.2
- Fixed an issue that forced the minSdkVersion to be 19 even though the SDK supports 14+.

### v2.0.1
- Crash reporting api key now being parsed correctly in JSON response

### v2.0
- Complete rebuild of the SDK to improve performance and architecture. See MIGRATION.md for
information on moving from the previous version of the SDK to 2.0 and above.

### v1.2.5
- Fixed a crash occurring in the WifiScanner.
- Removed a misleading logging message

### v1.2.4
- If an implementing project overrides and removes the BLUETOOTH or BLUETOOTH_ADMIN permission,
 the SDK no longer crashes and continues to work.

### v1.2.3
- Stopped the SDK from running on devices running Android less than 18.

### v1.2.2
- Fixed a crash when the SDK was used on Android versions less than SDK 18
- Possible fix to ASUS `WifiScanner` crash experienced by partners

### v1.2.1
- Fixed an issue where the Api Id and Api Secret were not being set correctly when using the builder
pattern resulting in an `IllegalArgumentException`.
- Fixed a crash that occurred when the user did not grant location permissions to the SDK.
- Documentation improvements on the `TamocoServiceBuilder`

### v1.2.0
- Builder pattern added for SDK initialisation
- Ability to add a listener to the SDK to receive JSON payloads sent from the server
- Manual control over when the SDK starts and stops.
- Update of SDK dependencies

### v1.1.0
- Parameter to `onApplicationCreate` changed from `Context` to `Application`. Your IDE should detect this and prompt you for the error.
- Added a foreground only mode option to stop the SDK being active when an application goes into the background.
- Enforced foreground only mode in Android Oreo.

### v1.0.0
- Converted to a gradle dependency
- Listen only mode. Allows the user to stop creatives being shown.
