![logo](https://bitbucket.org/repo/XX5xgGx/images/3404555888-logo.gif.png)
## SDK Migration
Even though the core features of the SDK remain the same, some of its APIs have changed
to offer performance improvements. This guide will allow you to migrate from version `1.X.X` to `2.0`.

## Improvements over the old SDK
This SDK was developed to solve some of the problems the old SDK had. The improvements include:
* Update to gradle 4.
* Battery consumption optimizations.
* Android O support.
* Doze Mode support.
* Simpler API.

## Unsupported features
Before starting, there are some features that have not been implemented yet in version `2.0`. This includes:
* QR Code Tracking Service
* Setting some configuration parameters. (which were delegated to the Remote settings)
* Enabling / disabling foreground only mode after the SDK is initialized.
* Enabling / disabling listen only mode after the SDK is initialized.
* Vault Service

## New requirements

### Minimum Android SDK Target
The SDK requires a minimum Android API level of 15 (Android 4.0.3 Ice Cream Sandwich).

```groovy
    minSdkVersion 15
    targetSdkVersion 27
```

**NOTE:** Beacon scanning will only work on devices with Android (4.3 Jelly Bean) and above. This is because BLE scanning requires at least API 18. The SDK gracefully stops beacon scanning if the API level is lower.

### Google Play Services
The Tamoco Android SDK relies on Google Play Services, most precisely on the Locations and Cloud Messaging packages. Any device without Google Play Services will be unable to use any of the SDK features.

The necessary dependencies are automatically included with the SDK out of the box. For more information on manually including the Google Play Services dependencies click [here](https://developers.google.com/android/guides/setup).

If you encounter any version conflicts in the Play Services versions imported by your app, please upgrade them to the latest stable version. The SDK currently uses `12.0.1`.

Example: 
```groovy
implementation 'com.google.android.gms:play-services-location:12.0.1'

```

### Support library
com.android.support:support-v4 is included in the SDK as a `compileOnly` dependency please include a version of 
said library in your project.

Example:
```groovy
implementation 'com.android.support:support-v4:27.1.0'

```
 
## New SDK Structure
The Tamoco Android SDK consists of 4 modules / kits:

* core: contains the basic features of the SDK and allows you to use the other modules.
* geofence: include this module if you would like to scan nearby geofences and detect geofence triggers.
* wifi: include this module if you would like to scan nearby wifi networks and detect wifi triggers.
* beacon: include this module if you would like to scan nearby beacons and detect beacon triggers.

The core module must **always** be included, while the other 3 modules inclusion depends on whether you need to scan for said triggers or not.

## Integration

1. Change the repository url to match the new maven repository:

    ```groovy
    	allprojects {
    	    repositories {
    	        jcenter()
    	        maven {
 	              ~~url "https://bitbucket.org/tamoco/tamoco-android-sdk/raw/releases"~~
    	            url "https://nexus.tamoco.com/repository/maven-public/"
    	        }
    	    }
    	}
    ```

2. Remove the old SDK dependency from your module's `build.gradle`:

	```groovy
    	dependencies {
    	    ...
    	    ~~implementation 'co.tamo.proximity:proximity:*.*'~~
    	    ...
    	}
	

3. Add the following dependency to your module's `build.gradle`:

	```groovy
	dependencies {
	    ...
	    implementation 'com.tamoco.sdk:core:2.1.8'
	    ...
	}
	```
	
## Initialization / Configuration
Version `2.0` simplifies the initialization and configuration process. 

Here is a summary of the changes made:

* The APP_ID and APP_SECRET have been moved to meta-data in the `Manifest.xml`.
* The `tamoco.properties` is no longer needed. All local configurations are 
made using the Builder pattern when initializing the SDK.
* All remaining configurations are fetched **ONLY** by using the Remote Settings.

Below you can find the whole new process:

1. Include the keys in your `Manifest.xml` as follows:

	```xml
	<application ...>
		...
	    <meta-data android:name="com.tamoco.APP_ID" android:value="{YOUR_APP_ID}"/>
	    <meta-data android:name="com.tamoco.APP_SECRET" android:value="{YOUR_APP_SECRET}"/>
		...
	</application>
	```
	
2. Create a custom Application class (or use the one you already had set up in your project).

3. Add the following code in your Application's `onCreate` method:

    ```java
    public class MyApplication extends Application {
        
        ...
        
        @Override
        public void onCreate() {
            super.onCreate();
            ...
            // Initialize the SDK
            Tamoco.with(getApplicationContext());
            ...
        }
        
        ...
    }
    ```
4. Alternatively, you can initialize the SDK using your local configurations as follows:

    ```java
    ...
        
    @Override
    public void onCreate() {
        super.onCreate();
        ...
        // Build the configuration
        TamocoConfig config = new TamocoConfig.Builder()
            /*
             * Determines if the SDK should start scanning as soon as it is initialized
             * or if an explicit call to Tamoco.startScanning(Context) is needed.
             *
             * true by default.
             */
            .startImmediately(true)
            /*
             * Determines if the SDK debug logs will be shown in Logcat or not.
             *
             * false by default.
             */
            .debugLogs(false)
            /*
             * Determines if the SDK will show debug notifications when hits occur or not.
             * 
             * false by default.
             */
            .foregroundOnly(false)
            /*
             * Determines if the SDK will only scan (without showing ads to the user) or not.
             * 
             * false by default.
             */
            .kits(new GeofenceKit(), new WifiKit(), new BeaconKit())
            .build();
            
        // Initialize the SDK using the configuration
        Tamoco.with(getApplicationContext(), config);
        ...
    }
    
    ... 
    ```

**NOTE:** You will need to include the respective modules for each scanning kits you want to use:

* `GeofenceKit`: geofence module.
* `WifiKit`: wifi module.
* `BeaconKit`: beacon module.


### Required permissions
The required permissions have been revised. Here is the new flow to ensure your app has all the permissions needed
by the sdk: 

#### Non-dangerous permissions
This permissions are auto approved by the user just by installing the app and only need to be declared 
in your app's `Manifest.xml`:

```xml
    <manifest ...>
        ...
        <!-- Needed to start the SDK when the device boots -->
        <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>
        <!-- Needed to update the inventory -->
        <uses-permission android:name="android.permission.INTERNET"/>
            
        <!-- Needed for wifi scanning -->
        <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
                
        <!-- Needed for beacon scanning -->
        <uses-permission android:name="android.permission.BLUETOOTH"/>
        <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
        ...
    </manifest>
```
You do not need to include the wifi and bluetooth permissions if you do not intend to scan for 
wifi networks and beacons respectively.

#### Dangerous permissions
This permissions also need to be added to your app's `Manifest.xml`:

```xml
    <manifest ...>
        ...
        <!-- Needed to know the devices current location, 
        the SDK will not work until this permission is granted -->
        <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
        <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
        ...
    </manifest>
```

However, you also need to request its approval at runtime. The Tamoco SDK offers a helper class to 
request all the required runtime permissions. We suggest including this requests on your Main or Splash Activity:

```java
public class MyMainActivity extends Activity {
    ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ...
        Tamoco.requestPermissions(this);
        ...
    }
    ...
}
```

You also need to notify the SDK when the permission request results are available so that it can start
to work as normal (if the permissions were granted).

```java
@Override
public class MyMainActivity extends Activity {
    ...
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ...
        Tamoco.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        ...
    }
    ...
}
```

Finally, you can always check if the required SDK permissions are granted or not, 
even thought you don't need to:

```java
if(Tamoco.hasSDKPermissions(getApplicationContext())){
    // Do something
} else {
    // Do other thing
}
```

You can read more about requesting permissions at Run Time in Google’s documentation: https://developer.android.com/training/permissions/requesting.html 

## Set a custom id
Although the functionality of this feature remain the same, its API has been modified as follows:
```java
// Old way
~~ProximityServiceManager.setCustomId(context, customId);~~ 
// New way
Tamoco.setCustomId(customId);
 ```

##Manual SDK Start / Stop 
This feature has remain the same but its API has changed as follows:

```java
// Old way
~~new TamocoServiceBuilder()~~
	~~.setApiIdAndSecret(API_ID,API_SECRET)~~
	~~.setStartImmediate(false)~~
	~~.build();~~

~~ProximityServiceManager.resumeTamoco((Application)this);~~

~~ProximityServiceManager.pauseTamoco((Application)this);~~
 
 
 

// New way
/*
 *  If you set startImmediate to false when initializing the configuration,
 *  the sdk wont start scanning until you call startScanning.
 */
TamocoConfig config = new TamocoConfig.Builder().startImmediately(false).build();

// Starts / restarts all scanning kits.
Tamoco.startScanning(context);

// Stops all scanning kits.
Tamoco.stopScanning(context);
```

##JSON Payload Callback 
You no longer need to provide a callback to be notified when a user taps a JSON Notification. 
Now, you can achieve the same using a `BroadcastReceiver`. 

First create the BroadcastReceiver:
```java
    mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Tamoco.ACTION_PAYLOAD_RECEIVED.equals(intent.getAction())) {
                String payload = intent.getStringExtra(Tamoco.EXTRA_PAYLOAD);
               // Do something with the JSON payload string
            }
        }
    };
```

Then you will need to register the receiver, there are two options for this.

1. Registering the receiver in your custom Application class will allow you to get notified even if your 
app has not launched yet or is running in background:

    ```java
    public class MyApplication extends Application {
        ...
        @Override
        public void onCreate() { 
            ...
            LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(Tamoco.ACTION_PAYLOAD_RECEIVED));
            ...
        }
        ...
    }
    ```

2. Registering the receiver in an Activity / Fragment will allow you to get notified only when the application is already
running in foreground:

    ```java
    public class MyActivity extends Activity {
        ...
        @Override
        public void onResume() { 
            super.onResume();
            ...
            LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(Tamoco.ACTION_PAYLOAD_RECEIVED));
            ...
        }
     
     
       @Override
       public void onPause() {   
           super.onPause();
           ...
           // Remember to unregister your receiver to avoid memory leaks.
           LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
           ...
       }
       ...
    }
    ```
    
## Listening to trigger hits
When using the previous SDK, all events where delivered to a single `BroadcastReceiver` using the 
`ProximityService.PROXIMITY_EVENT_ACTION` action. This broadcast have now been split in the different modules.
This is the new flow to listen to said hits:

First create the according `BroadcastReceiver` and then register / unregister it accordingly.

* Geofence: 

    ```java
    public class MyActivity extends Activity {
        ...
        
        @Override
        public void onCreate() {
            super.onCreate();
            ...
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (GeofenceBroadcast.ACTION_GEOFENCE_HIT.equals(intent.getAction())) {
                        GeofenceTrigger trigger = GeofenceBroadcast.parseTrigger(intent);
                        if (trigger != null) {   
                          // Do something with the trigger data
                        }
                    }
                }
            };   
        }
        
        @Override
        public void onResume() { 
            super.onResume();
            ...
            LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(GeofenceBroadcast.ACTION_GEOFENCE_HIT));
            ...
        }
     
     
       @Override
       public void onPause() {   
           super.onPause();
           ...
           // Remember to unregister your receiver to avoid memory leaks.
           LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
           ...
       }
       ...
    }
    ```

* Wifi: 
    ```java
    public class MyActivity extends Activity {
        ...
        
        @Override
        public void onCreate() {
            super.onCreate();
            ...
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (WifiBroadcast.ACTION_WIFI_HIT.equals(intent.getAction())) {
                        WifiTrigger trigger = WifiBroadcast.parseTrigger(intent);
                        if (trigger != null) {   
                          // Do something with the trigger data
                        }
                    }
                }
            };   
        }
        
        @Override
        public void onResume() { 
            super.onResume();
            ...
            LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(WifiBroadcast.ACTION_WIFI_HIT));
            ...
        }
     
     
       @Override
       public void onPause() {   
           super.onPause();
           ...
           // Remember to unregister your receiver to avoid memory leaks.
           LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
           ...
       }
       ...
    }
    ```

* Beacon: 

  ```java
  public class MyActivity extends Activity {
      ...
      
      @Override
      public void onCreate() {
          super.onCreate();
          ...
          mReceiver = new BroadcastReceiver() {
              @Override
              public void onReceive(Context context, Intent intent) {
                  if (BeaconBroadcast.ACTION_BEACON_HIT.equals(intent.getAction())) {
                      BeaconTrigger trigger = BeaconBroadcast.parseTrigger(intent);
                      if (trigger != null) {   
                        // Do something with the trigger data
                      }
                  }
              }
          };   
      }
      
      @Override
      public void onResume() { 
          super.onResume();
          ...
          LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(BeaconBroadcast.ACTION_BEACON_HIT));
          ...
      }
   
   
     @Override
     public void onPause() {   
         super.onPause();
         ...
         // Remember to unregister your receiver to avoid memory leaks.
         LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
         ...
     }
     ...
  }
  ```
  
You can determine which event provoked the hit by comparing `trigger.getLastEvent()` 
with the appropriate constant in the `Trigger` class:

**Geofence**:
* `EVENT_GEOFENCE_ENTER`: The user has entered the geofence.
* `EVENT_GEOFENCE_DWELL`: The user has remained enough time inside the geofence to consider him dwelling.
* `EVENT_GEOFENCE_EXIT`: The user has exited the geofence.

**Wifi**:
* `EVENT_WIFI_ENTER`: The user has entered the range of the wifi's network signal.
* `EVENT_WIFI_DWELL`: The user has remained enough time in the range of the wifi's network signal to consider him dwelling.
* `EVENT_WIFI_EXIT`: The user has exited the range of the wifi's network signal.
* `EVENT_WIFI_CONNECT`: The user has connected to the wifi network.
* `EVENT_WIFI_DISCONNECT`: The user has disconnected from the wifi network.

**Beacon**:
* `EVENT_BLE_ENTER`: The user has entered the range of the BLE beacon.
* `EVENT_BLE_HOVER`: The user is near the BLE beacon and has remained in its range for a certain amount of time.
* `EVENT_BLE_DWELL`: The user has remained enough time inside the BLE beacon's range to consider him dwelling.
* `EVENT_BLE_EXIT`: The user has exited the range of the BLE beacon.

**Analog constants for Eddystone beacons**:
* `EVENT_EDY_ENTER`
* `EVENT_EDY_HOVER`
* `EVENT_EDY_DWELL`
* `EVENT_EDY_EXIT`

You can also check which is the current proximity state of the trigger comparing `trigger.getState()`
with the appropriate constant in the `Trigger` class:

**Geofence**:
* `STATUS_GEOFENCE_OUTSIDE`: The user is currently outside of the geofence.
* `STATUS_GEOFENCE_INSIDE`: The user is currently inside the geofence.

**Wifi**:
* `STATUS_WIFI_OUT_OF_RANGE`: The user is out of the range of the wifi network's signal.
* `STATUS_WIFI_IN_RANGE`: The user is in the range of the wifi network's signal.

**Beacon**:
* `STATUS_BEACON_OUT_OF_RANGE`: The user is out of the range of the beacon.
* `STATUS_BEACON_IN_RANGE`: The user is in the range of the beacon.

 

## Compile with Third Party Services 

### PROGUARD
No special PROGUARD configuration is needed to use the SDK. Therefore you can remove the previously added ones:
```groovy
~~-keep class co.tamo.proximity.** { *; }~~
~~-keepclassmembers enum co.tamo.proximity.NearbyInventoryType { *; }~~
```





