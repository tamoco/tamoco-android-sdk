![logo](https://bitbucket.org/repo/XX5xgGx/images/3404555888-logo.gif.png)

### Android Client SDK Integration Guide

This document is written using Android Studio 3.3.2 as IDE.

If you are migrating from version `1.X.X` check [MIGRATION.md](MIGRATION.md) for the migration guide.

### Changelog
To see what has changed between each version of the SDK please check [CHANGELOG.md](CHANGELOG.md)

## Custom Configuration
Even though the initialization process in the previous step is the simplest one, it offers little utility.
You may want to configure the SDK before you initialize it, in this way you can set the parameters
you would like to override and specify which triggers you would like to scan.

To do this, provide a `TamocoConfig` instance to the `Tamoco.with` method. This instance can be created
using the builder pattern as follows:

```java
...

@Override
public void onCreate() {
    super.onCreate();
    ...
    // Build the configuration
    TamocoConfig config = new TamocoConfig.Builder()
        /*
         * Determines if the SDK should start scanning as soon as it is initialized
         * or if an explicit call to Tamoco.startScanning(Context) is needed.
         *
         * true by default.
         */
        .startImmediately(true)
        /*
         * Determines if the SDK debug logs will be shown in Logcat or not.
         *
         * false by default.
         */
        .debugLogs(false)
        /*
         * Determines if the SDK will only work when the app is in foreground or not.
         *
         * false by default.
         */
        .foregroundOnly(false)
        /*
         * Sets all the kits initialized with the SDK
         *
         * null by default.
         */
        .kits(new GeofenceKit(), new WifiKit(), new BeaconKit()) 
        .build();

    // Initialize the SDK using the configuration
    Tamoco.with(getApplicationContext(), config);
    ...
}

...
```

### Usage

## Control when the SDK performs scans
The SDK will continuously scan for triggers even if your application is not launched or running
in the background. Even though the SDK uses battery friendly ways of scanning, you may
still want to manually tell it when to scan. You can do this by calling the following methods:

```java
/*
 *  If you set startImmediate to false when initializing the configuration,
 *  the sdk wont start scanning until you call startScanning.
 */
TamocoConfig config = new TamocoConfig.Builder().startImmediately(false).build();

// Starts / restarts all scanning kits.
Tamoco.startScanning(context);

// Stops all scanning kits.
Tamoco.stopScanning(context);
```

## Set a custom id
The Tamoco SDK allows you to append an additional tracking attribute such as app user id to tracking events.
This attribute can be seen from the Tamoco Dashboard and can help you to better distinguish your audience.

To set this custom id use this snippet:
```java
Tamoco.setCustomId("myId");
```

## Listening to SDK events

### Inventory updates
In order to save battery, the SDK only monitors the triggers that are near the user. This monitored inventory
gets updated as the user navigates. You can get notified of this update by registering a `BroadcastReceiver` using
the LocalBroadcastManager and `Tamoco.ACTION_INVENTORY_UPDATED` as its IntentFilter:

```java
public class MyActivity extends Activity {
    ...

    @Override
    public void onCreate() {
        super.onCreate();
        ...
        mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (Tamoco.ACTION_INVENTORY_UPDATED.equals(intent.getAction())) {
                      // Do something when the inventory updates.
                    }
                }
            };
        ...
    }

    @Override
    public void onResume() {
        super.onResume();
        ...
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(Tamoco.ACTION_INVENTORY_UPDATED));
        ...
    }


   @Override
   public void onPause() {
       super.onPause();
       ...
       // Remember to unregister your receiver to avoid memory leaks.
       LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
       ...
   }
   ...
}
```

### Trigger hits
There are three types of triggers than can be hit in the SDK: Geofences, Wifi Networks and Beacons.
To detect hits on this triggers you will need to include their respective kit in your app.

Once a trigger gets hit (a significant event occurs with said trigger), you can get notified by using
BroadcastReceivers registered in the LocalBroadcastManager with their respective ActionFilters.

First create the according `BroadcastReceiver` and then register / unregister it accordingly.

* Geofence:

```java
public class MyActivity extends Activity {
    ...

    @Override
    public void onCreate() {
        super.onCreate();
        ...
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (GeofenceBroadcast.ACTION_GEOFENCE_HIT.equals(intent.getAction())) {
                    GeofenceTrigger trigger = GeofenceBroadcast.parseTrigger(intent);
                    if (trigger != null) {
                      // Do something with the trigger data
                    }
                }
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        ...
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(GeofenceBroadcast.ACTION_GEOFENCE_HIT));
        ...
    }


   @Override
   public void onPause() {
       super.onPause();
       ...
       // Remember to unregister your receiver to avoid memory leaks.
       LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
       ...
   }
   ...
}
```

* Wifi:

```java
public class MyActivity extends Activity {
    ...

    @Override
    public void onCreate() {
        super.onCreate();
        ...
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (WifiBroadcast.ACTION_WIFI_HIT.equals(intent.getAction())) {
                    WifiTrigger trigger = WifiBroadcast.parseTrigger(intent);
                    if (trigger != null) {
                      // Do something with the trigger data
                    }
                }
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        ...
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(WifiBroadcast.ACTION_WIFI_HIT));
        ...
    }
    
    

   @Override
   public void onPause() {
       super.onPause();
       ...
       // Remember to unregister your receiver to avoid memory leaks.
       LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
       ...
   }
   ...
}
```

* Beacon:

```java
public class MyActivity extends Activity {
  ...

  @Override
  public void onCreate() {
      super.onCreate();
      ...
      mReceiver = new BroadcastReceiver() {
          @Override
          public void onReceive(Context context, Intent intent) {
              if (BeaconBroadcast.ACTION_BEACON_HIT.equals(intent.getAction())) {
                  BeaconTrigger trigger = BeaconBroadcast.parseTrigger(intent);
                  if (trigger != null) {
                    // Do something with the trigger data
                  }
              }
          }
      };
  }

  @Override
  public void onResume() {
      super.onResume();
      ...
      LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(BeaconBroadcast.ACTION_BEACON_HIT));
      ...
  }


 @Override
 public void onPause() {
     super.onPause();
     ...
     // Remember to unregister your receiver to avoid memory leaks.
     LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
     ...
 }
 ...
}
```

You can determine which event provoked the hit by comparing `trigger.getLastEvent()`
with the appropriate constant in the `Trigger` class:

**Geofence**:
* `EVENT_GEOFENCE_ENTER`: The user has entered the geofence.
* `EVENT_GEOFENCE_DWELL`: The user has remained enough time inside the geofence to consider him dwelling.
* `EVENT_GEOFENCE_EXIT`: The user has exited the geofence.

**Wifi**:
* `EVENT_WIFI_ENTER`: The user has entered the range of the wifi's network signal.
* `EVENT_WIFI_DWELL`: The user has remained enough time in the range of the wifi's network signal to consider him dwelling.
* `EVENT_WIFI_EXIT`: The user has exited the range of the wifi's network signal.
* `EVENT_WIFI_CONNECT`: The user has connected to the wifi network.
* `EVENT_WIFI_DISCONNECT`: The user has disconnected from the wifi network.

**Beacon**:
* `EVENT_BLE_ENTER`: The user has entered the range of the BLE beacon.
* `EVENT_BLE_HOVER`: The user is near the BLE beacon and has remained in its range for a certain amount of time.
* `EVENT_BLE_DWELL`: The user has remained enough time inside the BLE beacon's range to consider him dwelling.
* `EVENT_BLE_EXIT`: The user has exited the range of the BLE beacon.

**Analog constants for Eddystone beacons**:
* `EVENT_EDY_ENTER`
* `EVENT_EDY_HOVER`
* `EVENT_EDY_DWELL`
* `EVENT_EDY_EXIT`

You can also check which is the current proximity state of the trigger comparing `trigger.getState()`
with the appropriate constant in the `Trigger` class:

**Geofence**:
* `STATUS_GEOFENCE_OUTSIDE`: The user is currently outside of the geofence.
* `STATUS_GEOFENCE_INSIDE`: The user is currently inside the geofence.

**Wifi**:
* `STATUS_WIFI_OUT_OF_RANGE`: The user is out of the range of the wifi network's signal.
* `STATUS_WIFI_IN_RANGE`: The user is in the range of the wifi network's signal.

**Beacon**:
* `STATUS_BEACON_OUT_OF_RANGE`: The user is out of the range of the beacon.
* `STATUS_BEACON_IN_RANGE`: The user is in the range of the beacon.

## Fetching the monitored inventory
Fetching the portion of the Tamoco inventory that is currently being monitored by the SDK is possible
by calling the designed method on each of the scanning kits. (you will need to add their modules to your application
if you wish to do so).

* Geofence:

```java
    GeofenceKit.getGeofenceTriggers(new TamocoRequestCallback<List<GeofenceTrigger>>() {
        @Override
        public void onResult(@Nullable List<GeofenceTrigger> result) {
            if (result != null) {
               // Do something with the monitored geofences
            }
        }
    });
```

* Wifi:

```java
    WifiKit.getWifiTriggers(new TamocoRequestCallback<List<WifiTrigger>>() {
        @Override
        public void onResult(@Nullable List<WifiTrigger> result) {
            if (result != null) {
               // Do something with the monitored wifi networks
            }
        }
    });
```
* Beacon

```java
    BeaconKit.getBeaconTriggers(new TamocoRequestCallback<List<BeaconTrigger>>() {
        @Override
        public void onResult(@Nullable List<BeaconTrigger> result) {
            if (result != null) {
               // Do something with the monitored beacons
            }
        }
    });
```
